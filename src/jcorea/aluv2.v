module aluV2//Takes all of funct 7 instead of a single mod bit and begin development of m extention
(
	input clock,
	input reset,
	
	input outputEnable,
	output reg [31:0] bus = 32'hz,
	
	//Register file and immediate direct input (saves clock cycles by avoiding bus transfers)
	input [31:0] aluPortRS1,//rs1 (always used)
	input [31:0] aluPortRS2,//rs2 (an immediate is used instead based if registerRegister = 0)
	input [31:0] immediate,//immediate value (used if registerRegister = 0)
	
	//Alu operation stuffs
	input [2:0] funct3,
	input registerRegister,//bit 5 of the opcode effectively
	input [6:0] funct7//bit 30 of the instruction which chooses between add/sub and srl/sra; ignored if funct3 is not 000 or 101
);
//Naming/selection
wire [31:0] valueX = aluPortRS1;
wire [31:0] valueY = registerRegister ? aluPortRS2 : immediate;
reg [31:0] result;

//Funct 7 interpretation
wire modOp = funct7[5];//sub instead of add or sra instead of srl
wire doSubtraction = modOp && registerRegister;//subtractions only happen in register-register operations
//wire mulOrDivInst = funct7[0];//used for multiplication and divisions instructions of the risc v m extension; 1 if the instrucion is of that specification

//Combination logic shared between operations to reduce lut waste or to allow selecting a specific range or specific bits
//wire [63:0] sharedSignedMultiplyOutput = multiply32Out64(valueX, valueY, 1);
//wire [63:0] mulhsuIntermediateOutput = multiply32Out64SignedByUnsigned(valueX, valueY);
//wire [63:0] mulhuIntermediateOutput = multiply32Out64(valueX, valueY, 0);

//Instruction execution combo clock
always @*//Things are commented out for now until the m extension is implemented
begin
	/*if (!mulOrDivInst)//Is an base specification instruction
	begin*/
		case (funct3)
			3'b000: result = addSub32(valueX, valueY, doSubtraction);//add/addi/sub
			3'b001: result = shiftLeftLogical32By5(valueX, valueY[4:0]);//sll/slli
			3'b010: result = setIfLessThan32(valueX, valueY, 1);//slt/slti
			3'b011: result = setIfLessThan32(valueX, valueY, 0);//sltu/sltiu
			3'b100: result = valueX ^ valueY;//xor/xori
			3'b101: result = shiftRight32By5(valueX, valueY, modOp);//srl/srli/sra/srai
			3'b110: result = valueX | valueY;//or/ori
			3'b111: result = valueX & valueY;//and/andi
			default: result = 32'h00000000;//will never happen
		endcase
	/*end
	else//is a multiply or a divide instruction//Commented out because at the moment these instructions are too slow and take up too many luts
	begin
		case (funct3)
			3'b000: result = sharedSignedMultiplyOutput[31:0];//mul
			3'b001: result = sharedSignedMultiplyOutput[63:32];//mulh
			3'b010: result = mulhsuIntermediateOutput[63:32];//mulhsu
			3'b011: result = mulhuIntermediateOutput[63:32];//mulhu
			3'b100: result = divide32(valueX, valueY, 1);//div
			3'b101: result = divide32(valueX, valueY, 0);//divu
			3'b110: result = remainder32(valueX, valueY, 1);//rem
			3'b111: result = remainder32(valueX, valueY, 0);//remu
			default: result = 32'h00000000;//will never happen
		endcase
	end*/
end

//Bus output stuffs
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		bus <= 32'hz;//take output off the bus
	end
	else if (clock)
	begin
		if (outputEnable)
		begin//it's ok if we're modifing a register that is being read from at the same time; bus is a register too so it will hold its output until outputEnable goes low or the next posedge of clock
			bus <= result;//output result to bus
		end
		else
		begin
			bus <= 32'hz;//take output off the bus
		end
	end
end

//Include math functions from common functions file
/* Math functions */ //todo implement these myself for practice instead of relying on verilog operators
//Note: simple functions (==, |, &, ^, !=, etc) won't be found here because they are trivial to implement and so they
//wouldn't be a good learning excersise/use of time
function automatic [31:0] addSub32(input [31:0] a, input [31:0] b, input subAB);//add/addi/sub
/*//My own implementation
integer i;
reg [31:0] bActual;
reg [32:0] carryIn;//Note: bit 32 goes unused
reg [32:0] ABCarryIn;//Note: bit 32 goes unused
reg [32:0] BCinCarryIn;//Note: bit 32 goes unused
reg [31:0] sumBCin;
begin
	//Conversion to twos complement for subtraction
	bActual = subAB ? ~b : b;//Invert all bits if subtracting
	carryIn[0] = subAB;//First carry in coming from subAB; effectively adding 1 if subtracting

	//Adding logic
	for (i = 0; i < 32; i = i + 1)
	begin
		  //Half adder 1 between b and carry in
		  sumBCin[i] = bActual[i] ^ carryIn[i];
		  BCinCarryIn[i+1] = bActual[i] & carryIn[i];//The carry in to the next full adder will be based on this
		  
		  //Half adder 2 between a and the sum of b and carry in
		  addSub32[i] = a[i] ^ sumBCin[i];
		  ABCarryIn[i+1] = a[i] & sumBCin[i];//The carry in to the next full adder will be based on this
		  
		  //Next carry is 1 if either half adder 1 or half adder 2 had an overflow (impossible for both to have one at the same time)
		  carryIn[i+1] = BCinCarryIn[i+1] | ABCarryIn[i+1];
	end
end*/
begin
	//alternative, probably better implementation (seems to use less luts and is probably more performant)
	addSub32 = subAB ? a - b : a + b;
end
endfunction

function automatic [31:0] shiftLeftLogical32By5(input [31:0] a, input [4:0] b);//sll/slli
begin
	shiftLeftLogical32By5 = a << b;
end
endfunction

function automatic [31:0] shiftRight32By5(input [31:0] a, input [4:0] b, input isArithmeticShift);//srl/srli/sra/srai
begin
	shiftRight32By5 =	isArithmeticShift ? a >>> b : a >> b;
end
endfunction

function automatic [31:0] setIfLessThan32(input [31:0] a, input [31:0] b, input isSigned);//slt/slti/sltu/sltiu
begin
	if (isSigned)
		setIfLessThan32 = ($signed(a) < $signed(b)) ? 32'h00000001 : 32'h00000000;
	else//unsigned
		setIfLessThan32 = (a < b) ? 32'h00000001 : 32'h00000000;
end
endfunction

/* //For now these functions are too slow and greedy for luts
function automatic [63:0] multiply32Out64(input [31:0] a, input [31:0] b, input isSigned);//mul/mulh/mulhu
begin
	if (isSigned)
		multiply32Out64 = $signed(a) * $signed(b);
	else//unsigned
		multiply32Out64 = a * b;
end
endfunction

function automatic [63:0] multiply32Out64SignedByUnsigned(input [31:0] a, input [31:0] b);//mul/mulh/mulhu
begin
	multiply32Out64SignedByUnsigned = $signed(a) * b;//a is signed, b is unsigned
end
endfunction

function automatic [31:0] divide32(input [31:0] a, input [31:0] b, input isSigned);//div/divu
begin
	if (isSigned)
		divide32 = $signed(a) / $signed(b);
	else//unsigned
		divide32 = a / b;
end
endfunction

function automatic [31:0] remainder32(input [31:0] a, input [31:0] b, input isSigned);//rem/remu
begin
	if (isSigned)
		remainder32 = $signed(a) % $signed(b);
	else//unsigned
		remainder32 = a % b;
end
endfunction*/

//Math function helpers (used internally by the above functions)

endmodule