module jcorea
(
	input clock,
	input reset,
	
	//CPU memory mapped io ports (for io dir, 0 = read, 1 = write)
	//Note that reads and writes are written to the addresses in little endian format
	//then converted back to be output / vise-versa for inputs
	//This makes it so for reads rd[0] = portXMemoryAddress[24] = portX[0]
	//and for writes............rs2[0] = portXMemoryAddress[24] = portX[0]
	//My recomendation is therefore that ports are accessed whole words at a time
	//but if you keep the little endian -> big endian format in mind you can write half words or bytes
	//Addresses for port read/write | io direction (0 for input, 1 for output)
	inout [31:0] portA,//FFFFFFF0   |   FFFFFFE0
	inout [31:0] portB,//FFFFFFF4   |   FFFFFFE4
	inout [31:0] portC,//FFFFFFF8   |   FFFFFFE8
	inout [31:0] portD,//FFFFFFFC   |   FFFFFFEC
	
	//Exposed port direction registers: Usefull for external modules to avoid multiple driver problems when compiling
	//portX[Y] can only be written to by an external moduleif it is not in output mode
	//if portXDir[y] is 1 then that pin is outputting data, if portXDir[y] is 0 then that pin is high impedance
	output [31:0] portADir,
	output [31:0] portBDir,
	output [31:0] portCDir,
	output [31:0] portDDir,
	
	output [31:0] busView,//to see the bus from outside modules for fun
	
	//Output for legacy asembly test programs that output to register 31; will be removed in later versions of jzjcore
	output [31:0] register31Output
	
);
parameter INITIAL_MEM_CONTENTS = "rom.mem";//File containing initial ram contents (32 bit words); execution starts from address 0x00000000
parameter RAM_A_WIDTH = 12;//number of addresses for code/ram (not memory mapped io); 2^RAM_A_WIDTH words = 2^RAM_A_WIDTH * 4 bytes

//L'autobus
wire [31:0] coreBus;
assign busView = coreBus;//to see the bus from outside modules for fun

//Register File (x0 to x31)
wire [4:0] registerWriteSelect = rd;//We never write to any register but rd
wire [4:0] registerOutputSelect;//I don't think this is needed for any RV32I instruction because we just use the side ports for most if not all register reads
wire registerFileWriteEnable;
wire [31:0] portRS1;
wire [31:0] portRS2;
registerFileV2 regFile (.clock(clock), .reset(reset), .registerWriteSelect(registerWriteSelect), .registerOutputSelect(registerOutputSelect), .writeEnable(registerFileWriteEnable),
								.busIO(coreBus), .portRS1RegSelect(rs1), .portRS2RegSelect(rs2), .portRS1(portRS1), .portRS2(portRS2), .register31Output(register31Output));

//Alu (instructions: add/addi/sub/sll/slli/slt/slti/sltu/sltiu/xor/xori/srl/srli/sra/srai/or/ori/and/andi)
wire aluOE;
wire registerRegisterMode;//Register-register alu opcodes are the same as immediate opcodes except for bit 5 of the opcode; if bit 5 is 1, control sets this wire to 1 as well
wire [31:0] aluImmediate = immediateI;//When there is an immediate for the alu, it will always be type I
aluV2 alu (.clock(clock), .reset(reset), .outputEnable(aluOE), .bus(coreBus), .aluPortRS1(portRS1), .aluPortRS2(portRS2),
			  .immediate(aluImmediate), .funct3(funct3), .registerRegister(registerRegisterMode), .funct7(funct7));
			
//Lui and auipc bus outputter/instruction handler (instructions: lui, auipc)
wire luiauipcEnable;
wire luiauipcOperation;//0 is lui, 1 is auipc
luiauipc luiauipcmodule (.clock(clock), .reset(reset), .bus(coreBus), .enable(luiauipcEnable), .operation(luiauipcOperation), .currentPC(currentPC), .immediateU(immediateU));

//Memory module (instructions:lw, lh, lhu, lb, lbu, sw, sh, sb)
wire memEnable;//Enable a memory command
wire [1:0] memOperationType;//Memory command to execute (l/s instruction, currentPC latch, output data at latched pc)
wire memReadReady;//ready for a read
wire memWriteReady;//ready for a write
wire instructionReady;//ready for another instruction fetch (memOperationType 3)
wire [31:0] directMemInstruction;
wire illegalMemOperation;//eg. unaligned access or bad funct3
memoryV6 #(.INITIAL_CONTENTS(INITIAL_MEM_CONTENTS), .A_WIDTH(RAM_A_WIDTH)) memoryModule (.clock(clock), .reset(reset), .bus(coreBus), .memReadReady(memReadReady),
			  .memWriteReady(memWriteReady), .memEnable(memEnable), .memOperationType(memOperationType), .funct3(funct3), .portRS1(portRS1), .portRS2(portRS2), .immediateI(immediateI),
			  .immediateS(immediateS), .currentPC(currentPC), .illegalOperation(illegalMemOperation), .instructionOut(directMemInstruction), .instructionReady(instructionReady),
			  .mmPortA(portA), .mmPortB(portB), .mmPortC(portC), .mmPortD(portD),  .mmPortADir(portADir), .mmPortBDir(portBDir), .mmPortCDir(portCDir), .mmPortDDir(portDDir));

//Program counter (used for instruction fetching and written to for jumping/branching instructions)
wire pcWE;
wire pcInc;//increment pc by 4 on next clock cycle
wire pcMisaligned;//1 if the program counter is not aligned to a 4 byte address
wire [31:0] currentPC;
programCounter pc (.clock(clock), .reset(reset), .writeEnable(pcWE), .increment(pcInc), .misaligned(pcMisaligned), .busIO(coreBus), .currentData(currentPC));

//Control transfer logic (jal/jalr/beq/bne/blt/bge/bltu/bgeu)
wire enableJumpLogic;
wire [1:0] jumpLogicOperationType;
wire outputNewPCJumpLogic;
jump jumpLogic (.clock(clock), .reset(reset), .bus(coreBus), .enable(enableJumpLogic), .operationType(jumpLogicOperationType), .outputPC(outputNewPCJumpLogic), .portRS1(portRS1),
					 .portRS2(portRS2), .funct3(funct3), .immediateJ(immediateJ), .immediateI(immediateI), .immediateB(immediateB), .currentPC(currentPC));

//Control logic (manages bus transfers and some instruction decoding)
//Instruction interpretation useful for certain modules (alu) that way
//the control logic dosen't have to worry about them
//However, modules shouldn't do anything without being signaled on
//their enable/control lines (eg. aluOE) because the control logic
//desides which to activate based on the opcode (which is not exposed intentionally)
wire [6:0] funct7;
wire [4:0] rs2;
wire [4:0] rs1;
wire [2:0] funct3;
wire [4:0] rd;
wire [31:0] immediateI;
wire [31:0] immediateS;
wire [31:0] immediateB;
wire [31:0] immediateU;
wire [31:0] immediateJ;
//Control logic executes some very simple instructions itself (fence/ecall/ebreak/fence.i)
controlV2 ctrl (.clock(clock), .reset(reset), .memReadReady(memReadReady), .memWriteReady(memWriteReady), .registerFileWE(registerFileWriteEnable),
					 .aluOE(aluOE), .aluRR(registerRegisterMode), .funct7(funct7), .rs2(rs2), .rs1(rs1), .funct3(funct3), .rd(rd), .outputNewPCJumpLogic(outputNewPCJumpLogic),
					 .immediateI(immediateI), .immediateS(immediateS), .immediateB(immediateB), .immediateU(immediateU), .immediateJ(immediateJ), .pcWE(pcWE), .pcInc(pcInc),
					 .memEnable(memEnable), .memOperationType(memOperationType), .instructionIn(directMemInstruction), .instructionReady(instructionReady), .luiauipcEnable(luiauipcEnable),
					 .luiauipcOperation(luiauipcOperation), .pcMisaligned(pcMisaligned), .illegalMemOperation(illegalMemOperation), .enableJumpLogic(enableJumpLogic),
					 .jumpLogicOperationType(jumpLogicOperationType));

endmodule