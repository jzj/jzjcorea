module programCounter
(
	input clock,
	input reset,
	
	input writeEnable,
	input increment,//increment by 4 on next clock cycle
	output misaligned,
	
	input [31:0] busIO,
	
	output [31:0] currentData
);

reg [31:0] pc = 32'h00000000;//start with nothing in it
assign misaligned = (pc % 4) != 0;//If this dosen't divide evenly, we have a problem
assign currentData = pc;//way other modules access the current data in the pc

always @(posedge clock, posedge reset)
begin
	if (reset)
		pc <= 32'h00000000;//reset pc to start of address space
	else if (clock)
	begin
		if (increment)//Going to next memory address
			 pc <= pc + 4;
		else if (writeEnable)//Reading in a new memory address (usefull for jumps/branches)
			 pc <= busIO;
	end
end

endmodule