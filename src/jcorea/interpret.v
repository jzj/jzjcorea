module instructionInterpret//everything except immediates and funct7 (only common between min 2 instruction types)
(
	input [31:0] instruction,
	
	//Note: not all of these will be valid at a given instant
	//It is expected the module user knows which instruction type
	//is being used and therefore which of these is valid
	output [6:0] funct7,
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [4:0] rd,
	output [6:0] opcode,//always valid
	
	//Immediates (already processed)
	output [31:0] immediateI,
	output [31:0] immediateS,
	output [31:0] immediateB,
	output [31:0] immediateU,
	output [31:0] immediateJ
);

assign funct7 = instruction[31:25];
interpretCommon otherDataInterpreter (.instruction(instruction), .rs2(rs2), .rs1(rs1), .funct3(funct3), .rd(rd), .opcode(opcode));//todo nuke other modules in this file and move all code into this one

//Immediates
assign immediateI[31:12] = {20{instruction[31]}};//sign extended
assign immediateI[11:0] = instruction[31:20];

assign immediateS[31:12] = {20{instruction[31]}};//sign extended
assign immediateS[11:0] = {instruction[31:25], instruction[11:7]};

assign immediateB[31:13] = {19{instruction[31]}};//sign extended
assign immediateB[12:1] = {instruction[31], instruction[7], instruction[30:25], instruction[11:8]};
assign immediateB[0] = 1'b0;//Multiples of 2 only

assign immediateU[31:12] = instruction[31:12];
assign immediateU[11:0] = 12'b000000000000;//both LUI and AUIPC zero out the bottom 12 bits, so we do that here

assign immediateJ[31:21] = {11{instruction[31]}};//sign extended
assign immediateJ[20:1] = {instruction[31], instruction[19:12], instruction[20], instruction[30:21]};
assign immediateJ[0] = 1'b0;//Multiples of 2 only

endmodule


/* Helper modules for interpreting instructions */
//used by instructionInterpret and interpretXtype functions
module interpretCommon//everything except immediates and funct7 (only common between min 2 instruction types)
(
	input [31:0] instruction,
	
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [4:0] rd,
	output [6:0] opcode
);

assign rs2 = instruction[24:20];
assign rs1 = instruction[19:15];
assign funct3 = instruction[14:12];
assign rd = instruction[11:7];
assign opcode = instruction[6:0];

endmodule

//These interpretXtype shouldn't be used as they are less lut efficient. Only use the above module instructionInterpret
//Also these aren't maintained so there may be bugs
module interpretRType
(
	input [31:0] instruction,
	
	output [6:0] funct7,
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [4:0] rd,
	output [6:0] opcode
);

assign funct7 = instruction[31:25];

interpretCommon(.instruction(instruction), .rs2(rs2), .rs1(rs1), .funct3(funct3), .rd(rd), .opcode(opcode));

endmodule

module interpretIType
(
	input [31:0] instruction,
	
	output [31:0] immediate,//sign extended
	output [4:0] rs1,
	output [2:0] funct3,
	output [4:0] rd,
	output [6:0] opcode
);

assign immediate[31:12] = instruction[31];//sign extended
assign immediate[11:0] = instruction[31:20];

interpretCommon(.instruction(instruction), .rs1(rs1), .funct3(funct3), .rd(rd), .opcode(opcode));

endmodule

module interpretSType
(
	input [31:0] instruction,
	
	output [31:0] immediate,
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [6:0] opcode
);

assign immediate[31:12] = instruction[31];//sign extended
assign immediate[11:0] = {instruction[31:25], instruction[11:7]};

interpretCommon(.instruction(instruction), .rs2(rs2), .rs1(rs1), .funct3(funct3), .opcode(opcode));

endmodule

module interpretBType
(
	input [31:0] instruction,
	
	output [31:0] immediate,
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [6:0] opcode
);

assign immediate[31:13] = instruction[31];//sign extended
assign immediate[12:1] = {instruction[31], instruction[7], instruction[30:25], instruction[11:8]};
assign immediate[0] = 1'b0;//Multiples of 2 only

interpretCommon(.instruction(instruction), .rs2(rs2), .rs1(rs1), .funct3(funct3), .opcode(opcode));

endmodule

module interpretUType
(
	input [31:0] instruction,
	
	output [31:0] immediate,
	output [4:0] rd,
	output [6:0] opcode
);

assign immediate[31:12] = instruction[31:12];
assign immediate[11:0] = 12'b000000000000;//both LUI and AUIPC zero out the bottom 12 bits, so we do that here

interpretCommon(.instruction(instruction), .rd(rd), .opcode(opcode));

endmodule

module interpretJType
(
	input [31:0] instruction,
	
	output [31:0] immediate,
	output [4:0] rd,
	output [6:0] opcode
);

assign immediate[31:21] = instruction[31];//sign extended
assign immediate[20:1] = {instruction[31], instruction[19:12], instruction[20], instruction[30:21]};
assign immediate[0] = 1'b0;//Multiples of 2 only

interpretCommon(.instruction(instruction), .rd(rd), .opcode(opcode));

endmodule