module luiauipc//Output proper values for the lui and auipc instructions to the bus to be latched into rd
(
	input clock,
	input reset,
	
	output reg [31:0] bus,
	
	input enable,//output to bus
	input operation,//0 is lui, 1 is auipc
	
	input [31:0] currentPC,//used for auipc
	input [31:0] immediateU//used for lui and auipc
);

always @(posedge clock, posedge reset)
begin
	if (reset)
		bus <= 32'hz;
	else if (clock)
	begin
		if (enable)//outputting to bus
		begin
			if (operation == 1'b0)//lui
				bus <= immediateU;//immediateU is already processed by the interpret module
			else//operation == 1 (auipc)
				bus <= (immediateU + (currentPC - 4));//instruction cycle increases the pc at the start, so we have to go back 4 addresses to get the address of the auipc instruction
		end
		else
			bus <= 32'hz;//no longer outputting to bus
	end
end

endmodule