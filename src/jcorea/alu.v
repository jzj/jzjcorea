module alu
(
	input clock,
	input reset,
	
	input outputEnable,
	output reg [31:0] bus = 32'hz,
	
	//Register file and immediate direct input (saves clock cycles by avoiding bus transfers)
	input [31:0] aluPortRS1,//rs1 (always used)
	input [31:0] aluPortRS2,//rs2 (an immediate is used instead based if registerRegister = 0)
	input [31:0] immediate,//immediate value (used if registerRegister = 0)
	
	//Alu operation stuffs
	input [2:0] funct3,
	input registerRegister,//bit 5 of the opcode effectively
	input mod//bit 30 of the instruction which chooses between add/sub and srl/sra; ignored if funct3 is not 000 or 101
);

//Naming/selection
wire [31:0] valueX = aluPortRS1;
wire [31:0] valueY = registerRegister ? aluPortRS2 : immediate;
reg [31:0] result;

//Instruction execution combo clock
always @*
begin
	case (funct3)//todo implement these myself for practice instead of relying on verilog operators
		3'b000: result = (mod && registerRegister) ? valueX - valueY : valueX + valueY;//add/addi/sub
		3'b001: result = valueX << valueY[4:0];//sll/slli
		3'b010: result = ($signed(valueX) < $signed(valueY)) ? 32'h00000001 : 32'h00000000;//slt/slti
		3'b011: result = (valueX < valueY) ? 32'h00000001 : 32'h00000000;//sltu/sltiu
		3'b100: result = valueX ^ valueY;//xor/xori
		3'b101: result = mod ? valueX >>> valueY[4:0] : valueX >> valueY[4:0];//srl/srli/sra/srai
		3'b110: result = valueX | valueY;//or/ori
		3'b111: result = valueX & valueY;//and/andi
		default: result = 32'h00000000;//will never happen
	endcase
end

//Bus output stuffs
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		bus <= 32'hz;//take output off the bus
	end
	else if (clock)
	begin
		if (outputEnable)
		begin//it's ok if we're modifing a register that is being read from at the same time; bus is a register too so it will hold its output until outputEnable goes low or the next posedge of clock
			bus <= result;//output result to bus
		end
		else
		begin
			bus <= 32'hz;//take output off the bus
		end
	end
end

endmodule