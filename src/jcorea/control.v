module control
(
	input clock,
	input reset,
	
	//inout reg [31:0] bus = 32'hz,//we acctually don't need a bus for any part of the instruction cycle or instructions (at least not so far)
	
	//Memory feedback
	input memReadReady,//ready for another read
	input memWriteReady,//ready for another write
	input instructionReady,//ready for another instruction fetch
	
	//Control lines (tell other modules to interpret various parts of the instruction themselves)
	output reg registerFileWE,//the data on the bus should be written to the value in rd
	output reg registerFileOE,//not used at the moment thanks to the very useful portRS1 and portRS2
	output reg aluOE,//Signals the type of opcode is for the alu so it can interpret the rest of the instruction itself (from funct7[5] and funct3)
	output reg aluRR,//Signals the alu opcode is a register register operation, not a register immediate operation
	output reg memEnable,//Signals a memory command (specified in memOperationType)
	output reg [1:0] memOperationType,//0 is a load instruction, 1 is a store instruction, 2 latches currentPC in a temp register, 3 reads data from the address contained in that temp register
	//output reg memWE,//Signals the type of opcode is a memory write and it should read from portRS2 to get the value to store and the s immediate/portRS1 to know the address
	//output reg memOE,//Signals the type of opcode is a memory read and it should write portRS2 to the address at portRS1 + the i immediate
	//output reg memPCOE,//Signals the memory module to use the currently latched program counter value as the adress and output the data at the address to the bus
	//output reg latchInPC,//Signals the memory module to latch in the contents of the bus into its mar
	output reg pcWE,//set pc to contents on bus
	output reg pcOE,//output pc to bus
	output reg pcInc,//increment pc by 4 next clock cycle
	
	//Instruction fetch directly from memory module
	input [31:0] instructionIn,
	
	//External export of instruction
	//Note no matter what this changes to modules cannot do anything without being told by the above control signals
	//They can do things internaly based on the below stuffsbut it must not affect their outward facing state or the
	//outward facing state of other modules
	output [6:0] funct7,
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [4:0] rd,
	//output [6:0] opcode,//the control logic is suppost to handle this, not other modules, so we don't expose it
	//Immediates
	output [31:0] immediateI,
	output [31:0] immediateS,
	output [31:0] immediateB,
	output [31:0] immediateU,
	output [31:0] immediateJ,
	
	output [31:0] test//testing
);
//testing
assign test = instructionRegister[6:0];

//Instruction stuff
wire [6:0] opcode;
reg [31:0] instructionRegister = 32'h00000000;
reg fetchInstAtNextPosedge = 1'b0;

//States
localparam STATE_PCINC_AND_MARSET = 3'b000;//Sets pc to increment by 4 and latches old value of pc into mar//"Fetch"
localparam STATE_GET_ADDRESS = 3'b001;//"Decode"
localparam STATE_EXECUTEA = 3'b010;//Not all execute steps will need to be used for most instructions//"Execute"
localparam STATE_EXECUTEB = 3'b011;
localparam STATE_EXECUTEC = 3'b100;
reg [2:0] currentState;
reg [2:0] nextState;
reg [2:0] statesNeeded;//shows how many states are needed by the instruction; 0 means only EXECUTEA, 1 means EXECUTEA and B, and so on

//State management
always @(negedge clock, posedge reset)
begin
	if (reset)
		currentState <= STATE_PCINC_AND_MARSET;
	else if (~clock)//we're trigering on the negative edge
		currentState <= nextState;//current_state becomes new_state each clock pulse
	else//This shouldn't be needed, but I am terrified of inferred latches so this is the anti-inferred-latches-warning superstisious object thing
		currentState <= STATE_PCINC_AND_MARSET;
end

//State transfer logic
always @*
begin
	case (currentState)
		STATE_PCINC_AND_MARSET:
		begin
			nextState = STATE_GET_ADDRESS;//Should only ever take 1 clock so we just move to the next state immediatly
		end
		STATE_GET_ADDRESS:
		begin
			if (instructionReady)
				nextState = STATE_EXECUTEA;//we will have read the instruction into the instructionRegister already as soon as memReadReady shows the memory has finished fetching it, so we can move to the next state
			else
				nextState = STATE_GET_ADDRESS;
		end
		STATE_EXECUTEA:
		begin
			if (statesNeeded == 0)
				nextState = STATE_PCINC_AND_MARSET;
			else
				nextState = STATE_EXECUTEB;
		end
		STATE_EXECUTEB:
		begin
			if (statesNeeded <= 1)
				nextState = STATE_PCINC_AND_MARSET;
			else
				nextState = STATE_EXECUTEB;
		end
		STATE_EXECUTEC:
		begin//we only have 3 execute states so far
			//if (statesNeeded <= 2)
				nextState = STATE_PCINC_AND_MARSET;
			//else
			//	nextState = STATE_EXECUTED;
		end
		default: nextState = STATE_PCINC_AND_MARSET;
	endcase
end

//State operations
always @(negedge clock, posedge reset)
begin
	if (reset)
	begin
		fetchInstAtNextPosedge <= 1'b0;
		registerFileWE <= 1'b0;
		registerFileOE <= 1'b0;
		aluOE <= 1'b0;
		aluRR <= 1'b0;
		memEnable <= 1'b0;
		memOperationType <= 2'b00;
		pcWE <= 1'b0;
		pcOE <= 1'b0;
		pcInc <= 1'b0;
	end
	else if (~clock)//we're trigering on the negative edge
	begin
		case (currentState)
			STATE_PCINC_AND_MARSET:
			begin
				//Reset whatever the instructions were doing
				fetchInstAtNextPosedge <= 1'b0;
				registerFileWE <= 1'b0;
				registerFileOE <= 1'b0;
				aluOE <= 1'b0;
				aluRR <= 1'b0;
				pcWE <= 1'b0;
				pcOE <= 1'b0;
			
				memEnable <= 1'b1;
				memOperationType <= 2'b10;//we copy the old data in the pc into the instruction mar...
				pcInc <= 1'b1;//at the same time we increment the pc
			end
			STATE_GET_ADDRESS:
			begin
				//Reset things from last state
				pcInc <= 1'b0;
				
				memEnable <= 1'b1;//give us our juicy instruction (yum!)
				memOperationType <= 2'b11;//will output instruction to instructionIn at the next posedge
				fetchInstAtNextPosedge <= 1'b1;//Yes, I'll take 1 instruction please!
			end
			STATE_EXECUTEA:
			begin
				//Reset things from last state
				memEnable <= 1'b0;
				memOperationType <= 2'b00;
				fetchInstAtNextPosedge <= 1'b0;
				
				case (opcode)//Now that we have the instruction, we can look at the opcode and tell modules to do things on the next clock!
					6'b0010011://Alu immediate operation instructions
					begin
						aluOE <= 1'b1;//Output result of alu (uses the i immediate and rs1 by default)
						registerFileWE <= 1'b1;//Save our result into rd
					end
					6'b0110011://Alu register-register operation instructions
					begin
						aluRR <= 1'b1;//Alu register-register operation
						aluOE <= 1'b1;//Output result of alu
						registerFileWE <= 1'b1;//Save our result into rd
					end
					default://todo make this halt the cpu
					begin
						fetchInstAtNextPosedge <= 1'b0;
						registerFileWE <= 1'b0;
						registerFileOE <= 1'b0;
						aluOE <= 1'b0;
						aluRR <= 1'b0;
						memEnable <= 1'b0;
						memOperationType <= 2'b00;
						pcWE <= 1'b0;
						pcOE <= 1'b0;
						pcInc <= 1'b0;
					end
				endcase
			end
		endcase
	end
end

//Instruction interpretation
instructionInterpret (.instruction(instructionRegister), .funct7(funct7), .rs2(rs2), .rs1(rs1), .funct3(funct3), .rd(rd), .opcode(opcode),
							 .immediateI(immediateI), .immediateS(immediateS), . immediateB(immediateB), .immediateU(immediateU), .immediateJ(immediateJ));
							 
//Instruction fetching
always @(posedge clock, posedge reset)
begin
	if (reset)
		instructionRegister <= 32'h00000000;
	else if (clock)
	begin
		if (fetchInstAtNextPosedge)
			instructionRegister <= instructionIn;
	end
end

//Number of execution states needed for different instructions
always @*
begin
	case (opcode)
		6'b0010011: statesNeeded = 0;
		6'b0110011: statesNeeded = 0;
		default statesNeeded = 0;
	endcase
end
endmodule