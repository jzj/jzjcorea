//Contains common functions that can be used by any module of the cpu (only automatic functions)

/* Endianness functions */
//Use wherever possible
//todo add functions for halfwords

function automatic [31:0] toLittleEndian(input [31:0] dataIn);
begin
	toLittleEndian = swapEndianness(dataIn);
end
endfunction

function automatic [31:0] toBigEndian(input [31:0] dataIn);
begin
	toBigEndian = swapEndianness(dataIn);
end
endfunction

function automatic [31:0] swapEndianness(input [31:0] dataIn);
begin
	swapEndianness = {dataIn[7:0], dataIn[15:8], dataIn[23:16], dataIn[31:24]};
end
endfunction

/* Bit extention functions (Sign and zero extention) */
//todo
