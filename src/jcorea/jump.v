module jump//handles unconditional jumps as well as branches
(
	input clock,
	input reset,
	
	output reg [31:0] bus,
	
	//Control cycle (takes 3 execution clock cycles; final "disable enable" can be handled in the STATE_PCINC_AND_MARSET section of the control module)
	//latch in currentPC using enable = 1 and operationType = 3
	//set operationType to the desired type of operation and set outputPC to 1 (then read it off the bus into the pc)
	//set outputPC and to 0 and read data off the bus into rd (only if jal or jalr, not for branch instructions)
	//disable enable
	
	//pc must be latched first as there is no buffering for rd and changing a register that is currently being read may affect bus output when outputPC = 1
	
	//Control lines
	input enable,
	input [1:0] operationType,//0 if jal, 1 if jalr, 2 if beq/bne/blt/bge/bltu/bgeu, 3 if latching in currentPC into an internal temp register
	input outputPC,//output the new pc address to the bus to be latched into the pc if 1, otherwise output new value for rd if 0 (ignored if operationType = 3 or if operation type = 2 and outputPC = 0)
	
	//data stuffs
	input [31:0] portRS1,//for branches and jalr
	input [31:0] portRS2,//for branches (used in comparisons)
	input [2:0] funct3,//decide between beq/bne/blt/bge/bltu/bgeu; for jal and jalr this is ignored
	input [31:0] immediateJ,//jal immediate
	input [31:0] immediateI,//jalr immediate
	input [31:0] immediateB,//immediate type for branch instructions
	input [31:0] currentPC//address of next instruction
);
//note to self; pc is always incremented at start of instruction cycle but the old pc is used during that cycle, so don't get confused. Set pc to
//what is needed and the instruction at that address (not the next) will be executed during that cycle

/* currentPC latching */
reg [31:0] tempCurrentPC;//holds value of currentPC when latched in with enable and operationType = 1
always @(posedge clock, posedge reset)
begin
	if (reset)
		tempCurrentPC <= 32'h00000000;
	else if (clock)
	begin
		if (enable && (operationType == 2'b11))
			tempCurrentPC <= currentPC;//latch in current pc
	end
end

/* Bus output */
always @(posedge clock, posedge reset)
begin
	if (reset)
		bus <= 32'hz;
	else if (clock)
	begin
		if (enable)
		begin
			if (operationType == 2'b00)//jal
				bus <= outputPC ? jalNewPC : jalNewRD;
			else if (operationType == 2'b01)//jalr
				bus <= outputPC ? jalrNewPC : jalrNewRD;
			else if (operationType == 2'b10)//branch instruction
			begin
				case (funct3)
					3'b000: bus <= beqNewPC;//beq
					3'b001: bus <= bneNewPC;//bne
					3'b100: bus <= bltNewPC;//blt
					3'b101: bus <= bgeNewPC;//bge
					3'b110: bus <= bltuNewPC;//bltu
					3'b111: bus <= bgeuNewPC;//bgeu
					default: bus <= 32'hz;
				endcase
			end
			else
				bus <= 32'hz;
		end
		else
			bus <= 32'hz;
	end
end

/* Combo logic */
//Unconditional jumps
//jal
wire [31:0] jalNewPC = immediateJ + (tempCurrentPC - 4);//currentPC already points to the next instruction, but we want to add the immediate to the address of this instruction
wire [31:0] jalNewRD = tempCurrentPC;//currentPC increments at the start of the instruction fetch cycle, so it already points to the next instruction
//jalr
wire [31:0] jalrPCIntermediateSum = immediateI + portRS1;
wire [31:0] jalrNewPC = {jalrPCIntermediateSum[31:1], 1'b0};//set lowest bit to 0
wire [31:0] jalrNewRD = tempCurrentPC;//currentPC increments at the start of the instruction fetch cycle, so it already points to the next instruction

//Branches//Todo implement myself for practice instead of relying on verilog operators
wire [31:0] branchNewPC = immediateB + (tempCurrentPC - 4);//The possible new pc is the same between all branch instructions; just the conditions are different
//If the condition is not met, then we just feed the program counter back its old value
wire [31:0] beqNewPC = (portRS1 == portRS2) ? branchNewPC : tempCurrentPC;
wire [31:0] bneNewPC = (portRS1 != portRS2) ? branchNewPC : tempCurrentPC;
wire [31:0] bltNewPC = ($signed(portRS1) < $signed(portRS2)) ? branchNewPC : tempCurrentPC;
wire [31:0] bgeNewPC = ($signed(portRS1) >= $signed(portRS2)) ? branchNewPC : tempCurrentPC;
wire [31:0] bltuNewPC = (portRS1 < portRS2) ? branchNewPC : tempCurrentPC;
wire [31:0] bgeuNewPC = (portRS1 >= portRS2) ? branchNewPC : tempCurrentPC;

endmodule