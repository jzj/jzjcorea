`timescale 1ns/1ps
module addSub32_tb;

reg [31:0] valueX = 32'h00000000;
reg [31:0] valueY = 32'h00000000;
reg subtract = 1'b0;
wire [31:0] functionOutput;
wire [31:0] expectedOutput = subtract ? valueX - valueY : valueX + valueY;

function automatic [31:0] addSub32(input [31:0] a, input [31:0] b, input subAB);//add/addi/sub
integer i;
reg [31:0] bActual;
reg [32:0] carryIn;//Note: bit 32 goes unused
reg [32:0] ABCarryIn;//Note: bit 32 goes unused
reg [32:0] BCinCarryIn;//Note: bit 32 goes unused
reg [31:0] sumBCin;
begin
	//Conversion to twos complement for subtraction
	bActual = subAB ? ~b : b;//Invert all bits if subtracting
	carryIn[0] = subAB;//First carry in coming from subAB; effectively adding 1 if subtracting

	//Adding logic
	for (i = 0; i < 32; i = i + 1)
	begin
		  //Half adder 1 between b and carry in
		  sumBCin[i] = bActual[i] ^ carryIn[i];
		  BCinCarryIn[i+1] = bActual[i] & carryIn[i];//The carry in to the next full adder will be based on this
		  
		  //Half adder 2 between a and the sum of b and carry in
		  addSub32[i] = a[i] ^ sumBCin[i];
		  ABCarryIn[i+1] = a[i] & sumBCin[i];//The carry in to the next full adder will be based on this
		  
		  //Next carry is 1 if either half adder 1 or half adder 2 had an overflow (impossible for both to have one at the same time)
		  carryIn[i+1] = BCinCarryIn[i+1] | ABCarryIn[i+1];
	end
	
	//alternative, probably better implementation (seems to use less luts and is probably more performant)
	//addSub32 = subAB ? a - b : a + b;
end
endfunction

assign functionOutput = addSub32(valueX, valueY, subtract);

initial
begin
    #10;
    valueX <= 32'hffffffff;
    valueY <= 32'hffffffff;
    
    #10;
    valueX <= 32'b00000000000000000000000000000111;
    valueY <= 32'b00000000100000000000000000000111;
    
    #10;
    valueX <= 32'h0000000f;
    valueY <= 32'h00000001;
    
    #10;
    subtract = 1'b1;
    
    #10;
    valueX <= 32'h00000000;
    valueY <= 32'h00000002;
    
    #10;
    valueX <= 32'b00000000000000000000000000100000;
    valueY <= 32'b00000000000000000000000000000001;
    
    #10;
    valueX <= 420;
    valueY <= 399;
end


initial//For dumping io
begin
    $dumpfile("addSub32_tb.vcd");
    $dumpvars(0, addSub32_tb);
    #1000 $finish;
end

endmodule
