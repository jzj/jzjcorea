`timescale 1ns/1ps
module updatedPortRegister_function_tb;

reg [31:0] rawPortRegisterTest = 32'h00000000;
reg [31:0] ioDirRegisterTest = 32'h00000000;
wire [31:0] functionOutput;


//Enabling/disabling outputs based on ioDirRegister
function automatic [31:0] updatedPortRegister(input [31:0] rawPortRegister, input [31:0] ioDirRegister);//If a bit in the io dir register is 0, then the corresponding bit in updatedPortRegister is set to z. Otherwise it is passed through
integer i;
begin
	for (i = 0; i < 32; i = i + 1)
	begin
        updatedPortRegister[i] = ioDirRegister[i] ? rawPortRegister[i] : 1'bz;//If the ioDirectionRegister is set to 1, then we are writing the data in rawPortRegister, otherwise we are reading by first setting the register to z
	end
end
endfunction

assign functionOutput = updatedPortRegister(rawPortRegisterTest, ioDirRegisterTest);

initial
begin
    #10;//nothing should happen becuase is still all 0
    rawPortRegisterTest <= 32'hffffffff;
    
    #10;//bottom 16 bits should now be output, top 16 should be z
    ioDirRegisterTest <= 32'h0000ffff;
    
    #10;
    rawPortRegisterTest <= 32'hffff0f0f;
    
    #10;//bottom 16 bits should now be output, top 16 should be z
    ioDirRegisterTest <= 32'hffffffff;
end


initial//For dumping io
begin
    $dumpfile("updatedPortRegister_function_tb.vcd");
    $dumpvars(0, updatedPortRegister_function_tb);
    #1000 $finish;
end

endmodule
