`timescale 1ns/1ps
module jump_tb;

reg clock = 1'b0;
wire reset = 1'b0;

wire [31:0] bus;

reg enableJumpLogic;
reg [1:0] jumpLogicOperationType;
reg outputNewPCJumpLogic;

reg [31:0] portRS1;
reg [31:0] portRS2;
reg [2:0] funct3;
reg [31:0] immediateJ;
reg [31:0] immediateI;
reg [31:0] immediateB;
reg [31:0] currentPC;//always points to the address of the next instruction because it is incremented at the start of the instruction fetch cycle

jump jumptest (.clock(clock), .reset(reset), .bus(bus), .enable(enableJumpLogic), .operationType(jumpLogicOperationType), .outputPC(outputNewPCJumpLogic), .portRS1(portRS1),
               .portRS2(portRS2), .funct3(funct3), .immediateJ(immediateJ), .immediateI(immediateI), .immediateB(immediateB), .currentPC(currentPC));

always
begin
    #10;
    clock <= ~clock;//Starts off, so negedge happens after 10, then posedge after another 10, and so on
end

reg [31:0] busLatchTest;//useful for determining if data is being latched correctly
always @(posedge clock)
begin
    busLatchTest <= bus;
end

initial//This effectively is the control logic of the cpu, so it only does things on the negedge
begin
    //0

    #20;
    
    //1//Store currentPC (works)
    currentPC <= 32'h00000008;
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b11;
    
    #20;
    
    //2//jal output pc (seems to work)
    currentPC <= 32'h00000000;//reset currentPC for now (already latched)
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b00;
    outputNewPCJumpLogic <= 1'b1;
    immediateJ = 32'h00000010;//0x10 + (previous currentPC - 4 = 0x4) = 0x14 output to bus
    
    #20;
    
    //3//jal output rd (seems to work)//(previous currentPC - 4) + 4 = 0x8) = 0x8 output to bus
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b00;
    outputNewPCJumpLogic <= 1'b0;//now we want to output rd
    immediateJ = 32'h00000000;//resetting this because it shouldn't be needed
    
    #20;
    
    //4//reset things using control lines
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b11;
    outputNewPCJumpLogic <= 1'b0;
    currentPC <= 32'h00000000;//resetting tempCurrentPC
    
    #20;
    
    //5//reset control lines
    enableJumpLogic <= 1'b0;
    jumpLogicOperationType <= 2'b00;
    outputNewPCJumpLogic <= 1'b0;
    
    #20;
    
    //6//Store currentPC again for testing jalr
    currentPC <= 32'h00000008;
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b11;
    
    #20;
    
    //7//Set immediateI, portRS1, and outputNewPCJumpLogic to test new pc output of jalr
    currentPC <= 32'h00000000;//reset currentPC for now (already latched)
    portRS1 <= 32'h00000008;
    immediateI <= 32'h00000009;
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b01;
    outputNewPCJumpLogic <= 1'b1;
    
    #20;
    
    //8//keeping all values same, but now outputting new rd
    portRS1 <= 32'h00000008;//not changing
    immediateI <= 32'h00000009;//not changing
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b01;
    outputNewPCJumpLogic <= 1'b0;
    
    #20;
    
    //9//reset things using control lines
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b11;
    outputNewPCJumpLogic <= 1'b0;
    currentPC <= 32'h00000000;//resetting tempCurrentPC
    portRS1 <= 32'h00000000;
    immediateI <= 32'h00000000;
    
    
    #20;
    
    //10//reset control lines
    enableJumpLogic <= 1'b0;
    jumpLogicOperationType <= 2'b00;
    outputNewPCJumpLogic <= 1'b0;
    
    #20;
    
    //11//latch currentPC
    currentPC <= 32'h00000008;
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b11;
    
    #20;
    
    //12//set control lines for beq to output PC (note outputPC is ignored so it is not bothered being set)
    currentPC <= 32'h00000000;//can reset currentPC now
    enableJumpLogic <= 1'b1;
    jumpLogicOperationType <= 2'b10;
    immediateB <= 32'h0000010;
    portRS1 <= 32'h98abcdef;
    portRS2 <= 32'h98abcdef;
    funct3 <= 3'b000;//beq
    
    #20;
    
    //13//make registers not equal to test false condition
    portRS1 <= 32'h98abcdef;
    portRS2 <= 32'h980bcdef;
    
    #20;
    
    //14//set control lines to bne
    funct3 <= 3'b001;//bne
    
    #20;
    
    //15//make registers equal to test bne false condition
    portRS1 <= 32'h98abcdef;
    portRS2 <= 32'h98abcdef;
    
    #20;
    
    //16//switch operation to blt (delicious) and change registers
    funct3 <= 3'b100;//blt
    portRS1 <= 32'hFFFFFFF6;//-10
    portRS2 <= 32'h0000000e;//EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    
    #20;
    
    //17//set registers equal to ensure blt is now false
    portRS1 <= 32'h98abcdef;
    portRS2 <= 32'h98abcdef;
    
    #20;
    
    //18//set rs1 higher than rs2
    portRS1 <= 32'h00000010;
    portRS2 <= 32'h0000000f;
    
    #20;
    
    //19//change operation to bltu
    funct3 <= 3'b110;//bltu
    
    #20;
    
    //20//make registers equal again
    portRS1 <= 32'h98abcdef;
    portRS2 <= 32'h98abcdef;
    
    #20;
    
    //21//make rs1 negative and less than portRS2
    portRS1 <= 32'hFFFFFFF6;//-10
    portRS2 <= 32'h0000000e;//EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    
    #20;
    
    //22//make rs1 positive but still less than rs2
    portRS1 <= 32'h0000000a;
    portRS2 <= 32'h0000000e;//EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    
    //todo test bge and bgeu
    
end

initial//For dumping io
begin
    $dumpfile("jump_tb.vcd");
    $dumpvars(0, jump_tb);
    #1000 $finish;
end

endmodule

/* What should happen at each state */
//Note: #.5 means the posedge after that step in the initial block (effectively these numbers should be multiplied by 20ns)
//Note: (=#) means the value a particular thing should be at that point
//0 Start of fpga, nothing should have happened
//0.5 No changes to control lines, nothing should have happened
//1.5 tempCurrentPC inside of jumptest should have latched currentPC (=0x8), jalNewRD should == tempCurrentPC (=0x8), jalNewPC should == (tempCurrentPC - 0x4) + immediateJ (=0x4)
//2 jalNewPC should still == (tempCurrentPC - 0x4) + immediateJ, which is now (=0x14) because of the change to immediateJ
//2.5 bus should output the value of jalNewPC (=0x14) until 3.5
//3.5 bus should output the value of jalNewRD which is still (=0x8) until 4.5
//4.5 output should be taken off bus and tempCurrentPC should be reset (jalNewRD and jalNewPC now are garbage)
//5 control lines should be reset
//5.5 nothing should happen
//6.5 tempCurrentPC inside of jumptest should have latched currentPC (=0x8),  jalrNewRD should == tempCurrentPC (=0x8)
//7 jalrPCIntermediateSum should ==  portRS1 + immediateI (=0x11), and jalrNewPC should == jalrPCIntermediateSum with lowest bit set to 0 (=0x10)
//7.5 jalrNewPC should be output to bus (=0x10)
//8.5 jalrNewRD should be output to bus (=0x8)
//9 jalrNewPC should be reset (=0x0), jalrPCIntermediateSum should be reset (=0x0)
//9.5 jalrNewRD and tempCurrentPC should both equal (=0x0), output should be taken off bus
//10 control lines should be reset
//10.5 nothing should happen
//11.5 tempCurrentPC should have latched currentPC and dependentant wires should have changed
//12.5 beq is true, bus should be immediateB + (tempCurrentPC - 4) (=0x14)
//13.5 beq is now false, bus should be (tempCurrentPC -4) + 4 (=0x8)
//14.5 branch operation is set to bne, which makes the previous false register comparison true so bus should be immediateB + (tempCurrentPC - 4) (=0x14)
//15.5 registers now equal again, which makes bne false so bus should be (tempCurrentPC -4) + 4 (=0x8)
//16.5 branch operation is set to blt and register are changed to make rs1 less than rs2. bus should be immediateB + (tempCurrentPC - 4) (=0x14)
//17.5 registers are now equal so blt is false so bus should be (tempCurrentPC -4) + 4 (=0x8)
//18.5 rs1 is now greater than rs2 and blt is still false so bus should be (tempCurrentPC -4) + 4 (=0x8)
//19.5 operation was changed to bltu, should have no effect; bus should be (tempCurrentPC -4) + 4 (=0x8)
//20.5 registers were made equal, bltu is still false so bus should be (tempCurrentPC -4) + 4 (=0x8)
//21.5 rs1 was made negative and rs2 was made positive. Although rs1 is less than rs2, bltu is unsigned so this will still be false; bus should be (tempCurrentPC -4) + 4 (=0x8)
//22.5 at long last, both rs1 and rs2 are positive while rs1 is less than rs2. bltu is true, therefore bus should be immediateB + (tempCurrentPC - 4) (=0x14)
