# JZJCoreA

My first RV32IZifencei implementation in Verilog using Quartus Prime as an ide

## Cycle counts for instructions

Note: raw cycle counts do not include initial instruction fetch clock cycles which add 3 clocks delay to each instruction

| Instruction | Raw Cycle Count | Cycles + Inst Fetch |
|:------------|:-----------------------|:--------------------|
|-Base Spec(I)-|
| lui | 2 | 5 |
| auipc | 2 | 5 |
| jal | 4 | 7 |
| jalr | 4 | 7 |
| beq | 3 | 6 |
| bne | 3 | 6 |
| blt | 3 | 6 |
| bge | 3 | 6 |
| bltu | 3 | 6 |
| bgeu | 3 | 6 |
| lb | 3 | 6 |
| lh | 3 | 6 |
| lw | 3 | 6 |
| lbu | 3 | 6 |
| lhu | 3 | 6 |
| sb | 2 | 5 |
| sh | 2 | 5 |
| sw | 1 | 4 |
| addi | 2 | 5 |
| slti | 2 | 5 |
| sltiu | 2 | 5 |
| xori | 2 | 5 |
| ori | 2 | 5 |
| andi | 2 | 5 |
| slli | 2 | 5 |
| srli | 2 | 5 |
| srai | 2 | 5 |
| add | 2 | 5 |
| sub | 2 | 5 |
| sll | 2 | 5 |
| slt | 2 | 5 |
| sltu | 2 | 5 |
| xor | 2 | 5 |
| srl | 2 | 5 |
| sra | 2 | 5 |
| or | 2 | 5 |
| and | 2 | 5 |
| fence | 1 | 4 |
| ecall | 1 | 4 |
| ebreak | 1 | 4 |
|-Zifencei-|
| fence.i | 1 | 4 |

## Memory Map

Note: addresses are inclusive, bounds not specified are not addressed to anything, and execution starts at 0x00000000

| Bytewise Address (whole word) | Physical Word-wise Address | Function |
|:------------------------------|:---------------------------|:---------|
|0x00000000 to 0x00000003|0x00000000|RAM Start|
|0x0000FFFC to 0x0000FFFF|0x00003fff|Ram End|
|0xFFFFFFE0 to 0xFFFFFFE3|0x3FFFFFF8|Memory Mapped IO Registers Start|
|0xFFFFFFFC to 0xFFFFFFFF|0x3FFFFFFF|Memory Mapped IO Registers End|
